#
# ovmf test cases using direct kernel boot.
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestCore import Edk2TestCore

class TestKernel(Edk2TestCore):

    timeout = 20

    def test_aa64_flash(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=group:kernel
        """
        self.common_prepare('aarch64', 'virt')
        self.common_add_flash('Build/ArmVirtQemu-AARCH64/DEBUG_*/FV/QEMU_EFI.fd',
                              'Build/ArmVirtQemu-AARCH64/DEBUG_*/FV/QEMU_VARS.fd')
        self.common_boot_kernel()
