#
# ovmf test cases, tpm, aa64
#

import os
import time
import logging
import subprocess

# avocado
import avocado
import avocado.utils.process

# local
from Edk2TestCore import Edk2TestCore, STREAM8_AA64

class TestSB(Edk2TestCore):

    timeout = 60

    def common_prepare_distro(self, disk):
        self.common_prepare('aarch64', 'virt')
        self.common_add_flash('Build/ArmVirtQemu-AARCH64/DEBUG_*/FV/QEMU_EFI.fd',
                              'Build/ArmVirtQemu-AARCH64/DEBUG_*/FV/QEMU_VARS.fd')
        self.common_add_virtio_scsi_pcie()
        self.common_add_qcow2_disk(disk)
        self.common_add_disk('scsi-hd')

    @avocado.skipUnless(os.path.exists("/usr/bin/swtpm"), 'no swtpm')
    @avocado.skipUnless(os.path.exists(STREAM8_AA64), 'no centos stream 8 image')
    def test_ovmf_tpm2_tis_stream8(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:tpm
        """
        self.common_prepare_distro(STREAM8_AA64)
        if not self.has_tpm2:
            self.cancel("no tpm2 support")

        self.common_add_tpm(True, 'tpm-tis')
        self.common_launch()

        self.common_linux_distro_login()
        self.common_linux_shell_init()
        self.common_linux_check_tpm(True)
        self.common_linux_shutdown()

    @avocado.skipUnless(os.path.exists("/usr/bin/swtpm"), 'no swtpm')
    @avocado.skipUnless(os.path.exists(STREAM8_AA64), 'no centos stream 8 image')
    def test_ovmf_tpm2_crb_stream8(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:tpm
        """
        self.common_prepare_distro(STREAM8_AA64)
        if not self.has_tpm2:
            self.cancel("no tpm2 support")

        self.common_add_tpm(True, 'tpm-crb')
        self.common_launch()

        self.common_linux_distro_login()
        self.common_linux_shell_init()
        self.common_linux_check_tpm(True)
        self.common_linux_shutdown()
