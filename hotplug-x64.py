#
# ovmf test cases, hotplug, x64
#

import os
import time
import logging

# avocado
import avocado

# local
from Edk2TestCore import Edk2TestCore

@avocado.skipUnless(os.path.exists('buildroot/efidisk-x86_64/efidisk.qcow2'), 'no disk image')
class TestDisk(Edk2TestCore):

    timeout = 60
    ports = []

    def common_prepare_x64(self, machine, ports):
        self.common_prepare('x86_64', machine)
        self.common_add_flash('Build/OvmfX64/DEBUG_*/FV/OVMF_CODE.fd',
                              'Build/OvmfX64/DEBUG_*/FV/OVMF_VARS.fd')
        if self.sb_enabled:
            self.cancel("sb is enabled")
        self.common_add_qcow2_disk('buildroot/efidisk-x86_64/efidisk.qcow2')
        self.common_add_virtio_blk_pcie()
        for i in range(31):
            self.ports.append(self.common_add_pcie_port())

    def common_hot_add(self, name, device, bus):
        self.vm.command('device_add',
                        id = name,
                        driver = device,
                        bus = bus)
        time.sleep(1)

    def test_ovmf_pcie_many_ports(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        """
        self.common_prepare_x64('q35', 31)

        self.common_launch()
        self.common_linux_buildroot_init()

        self.console_send('lspci -vt')
        self.console_wait('---root---')

        self.common_linux_shutdown()

    def test_ovmf_pcie_hotplug_scsi(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        """
        self.common_prepare_x64('q35', 3)

        self.common_launch()
        self.common_linux_buildroot_init()

        self.common_hot_add('hotplug', 'virtio-scsi-pci', self.ports[0])
        self.console_send('lspci -vt')
        self.console_wait('---root---')

        self.common_linux_shutdown()
