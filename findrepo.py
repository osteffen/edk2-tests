#!/usr/bin/python
import os
import sys
import optparse

default = {
    'fedora'  : 'https://download.fedoraproject.org/pub/fedora/linux/releases/%s/',
    'stream8' : 'http://mirror.centos.org/centos/%s-stream/',
    'stream9' : 'http://mirror.stream.centos.org/%s-stream/',
}

kraxel = {
    'fedora'  : 'http://spunk.home.kraxel.org/mirror/fedora/rsync/f%s-release/',
    'stream8' : 'http://spunk.home.kraxel.org/mirror/stream/%s-stream/',
    'stream9' : 'http://spunk.home.kraxel.org/mirror/stream/%s-stream/',
    'rhel7'   : 'http://spunk.home.kraxel.org/mirror/rhel/redhat/rhel-7/rel-eng/RHEL-7/latest-RHEL-%s/compose/',
    'rhel8'   : 'http://sirius.home.kraxel.org/rhel-8/rel-eng/RHEL-8/latest-RHEL-%s/compose/',
}

redhat = {
    'fedora'  : 'http://download.devel.redhat.com/released/fedora/F-%s/GOLD/',
    'stream8' : 'http://download.devel.redhat.com/released/CentOS/centos/%s-stream/',
    'rhel7'   : 'http://download.devel.redhat.com/released/RHEL-7/%s/',
    'rhel8'   : 'http://download.devel.redhat.com/released/RHEL-8/%s/',
}

def getoverride():
    hostname = os.uname()[1]
    if 'kraxel.org' in hostname:
        return kraxel
    if 'redhat.com' in hostname:
        return redhat
    return {}

def findrepo(distro, version):
    hostname = os.uname()[1]
    override = getoverride()
    if override[distro] is not None:
        return override[distro] % version
    elif default[distro] is not None:
        return default[distro] % version
    return None

if __name__ == '__main__':
    parser = optparse.OptionParser()
    parser.add_option('-d', '--distro', dest = 'distro', type = 'string',
                      help = 'find repo for DISTRO', metavar = 'DISTRO')
    parser.add_option('-v', '--version', dest = 'version', type = 'string',
                      help = 'use distro version VERSION', metavar = 'VERSION')
    parser.add_option('-l', '--list', dest = 'listmode', action = 'store_true', default = False,
                      help = 'list known distros')
    (options, args) = parser.parse_args()

    if options.listmode:
        override = getoverride()
        for item in override.keys():
            print(f"override: {item}")
        for item in default.keys():
            print(f"default: {item}")
        sys.exit(0)

    if options.distro is None:
        print("no distro given (use -d or --distro)")
        sys.exit(1)      

    if options.version is None:
        if options.distro == 'fedora':
            options.version = '35'
        if options.distro == 'stream8':
            options.version = '8'
        if options.distro == 'stream9':
            options.version = '9'
        if options.distro == 'rhel8':
            options.version = '8.5.0'
        if options.distro == 'rhel7':
            options.version = '7.9'
        else:
            print("no version given (use -v or --version)")
            sys.exit(1)      

    print(findrepo(options.distro, options.version))
