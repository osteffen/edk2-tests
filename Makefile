
# buildroot
ARCH	?= aarch64 arm i386 x86_64
TARS	:= $(patsubst %,buildroot/efidisk-%.tar.xz,$(ARCH))
TARS	+= $(patsubst %,buildroot/initrd-%.tar.xz,$(ARCH))
DIRS	:= $(patsubst %,buildroot/efidisk-%,$(ARCH))
DIRS	+= $(patsubst %,buildroot/initrd-%,$(ARCH))

# virt-install
HOST_ARCH	:= $(shell uname -m)
PASSWD_FILE	:= distros/admin-password
AVOCADO_RUN	:= avocado run --test-runner=runner

DISTRO_IMGS	+= distros/fedora-$(HOST_ARCH).qcow2
DISTRO_IMGS	+= distros/stream8-$(HOST_ARCH).qcow2
DISTRO_IMGS	+= distros/stream9-$(HOST_ARCH).qcow2

########################################################################
# misc

default: help

help:
	@echo ""
	@echo "misc targets:"
	@echo "  help            - print this text"
	@echo "  tags            - list avocado tags"
	@echo "  clean           - remove backup files"
	@echo ""
	@echo "buildroot targets:"
	@echo "  fetch           - fetch buildroot tarballs"
	@echo "  fetch-grub      - fetch efi binaries (grub & friends, centos stream 9)"
	@echo "  boot-iso        - create boot isos"
	@echo "  extract         - extract buildroot tarballs"
	@echo "  clean-buildroot - remove all buildroot bits"
	@echo ""
	@echo "distro targets:"
	@echo "  distros         - autoinstall distros"
	@echo "  clean-distros   - remove distro images"
	@echo ""

tags:
	cat *.py | awk '/tags=/ { print $$2 }' | sed -e 's/tags=/ /' | sort | uniq -c

clean:
	rm -f *~ tools/*~ kickstart/*~

python:
	./python-setup.sh

########################################################################
# buildroot

extract: $(DIRS) $(TARS)

fetch download: $(TARS)

.phony: fetch-grub fetch-grub-aarch64 fetch-grub-x86_64
fetch-grub: fetch-grub-aarch64 fetch-grub-x86_64

fetch-grub-aarch64: buildroot/initrd-aarch64
	./tools/grub-fetch.sh buildroot/initrd-aarch64/EFI/BOOT aarch64

fetch-grub-x86_64: buildroot/initrd-x86_64
	./tools/grub-fetch.sh buildroot/initrd-x86_64/EFI/BOOT x86_64

.phony: boot-iso boot-iso-aarch64 boot-iso-x86_64
boot-iso: boot-iso-aarch64 boot-iso-x86_64

boot-iso-aarch64: fetch-grub-aarch64
	./tools/make-boot-iso.sh buildroot/initrd-aarch64

boot-iso-x86_64: fetch-grub-x86_64
	./tools/make-boot-iso.sh buildroot/initrd-x86_64

buildroot/%: buildroot/%.tar.xz
	(cd buildroot; rm -rf $*; tar xf $*.tar.xz)

buildroot/%-aarch64.tar.xz:
	./buildroot-fetch.sh $*-aarch64 aarch64

buildroot/%-arm.tar.xz:
	./buildroot-fetch.sh $*-arm arm

buildroot/%-i386.tar.xz:
	./buildroot-fetch.sh $*-i386 x86

buildroot/%-x86_64.tar.xz:
	./buildroot-fetch.sh $*-x86_64 x64

clean-buildroot:
	rm -rf $(DIRS) $(TARS)

########################################################################
# distros (virt-install)

.phony: distros
distros: $(DISTRO_IMGS)

distros/%-$(HOST_ARCH).qcow2: $(PASSWD_FILE)
	$(AVOCADO_RUN) install.py -t arch:$(HOST_ARCH),distro:$*

clean-distros:
	rm -rf $(DISTRO_IMGS)

$(PASSWD_FILE):
	mkdir -p $$(dirname $@)
	./tools/pwgen.sh > $@
