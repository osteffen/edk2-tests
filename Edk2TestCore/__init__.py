import os
import glob
import time
import shutil
import logging
import subprocess

# avocado
import avocado

# qemu
from qemu.machine import QEMUMachine

# distro image config
PASSWD_FILE  = "distros/admin-password"
FEDORA_X64   = "distros/fedora-x86_64.qcow2"
FEDORA_AA64  = "distros/fedora-aarch64.qcow2"
STREAM8_X64  = "distros/stream8-x86_64.qcow2"
STREAM8_AA64 = "distros/stream8-aarch64.qcow2"
STREAM9_X64  = "distros/stream9-x86_64.qcow2"
STREAM9_AA64 = "distros/stream9-aarch64.qcow2"
RHEL7_X64    = "distros/rhel7-x86_64.qcow2"
RHEL8_X64    = "distros/rhel8-x86_64.qcow2"
RHEL8_AA64   = "distros/rhel8-aarch64.qcow2"

# init
QEMU_BINARY = os.environ.get("QEMU_BINARY");
WORKSPACE = os.environ.get("WORKSPACE");

class Edk2TestCore(avocado.Test):

    #
    # init + cleanup
    #

    vm = None
    nsenter = None
    swtpm = None
    pcie = 0
    cplist = {}
    rmlist = []

    def tearDown(self):
        if self.vm is not None:
            self.log.info("### tear down guest")
            try:
                self.vm.get_qmp_events()
                self.vm.shutdown()
            except:
                pass
        if self.swtpm is not None:
            self.log.info('### stop swtpm')
            self.swtpm.stop()
        self.log.info("### copy files")
        for src, dst in self.cplist.items():
            self.log.info("### - " + src + " -> " + dst)
            shutil.copyfile(src, dst)
        self.log.info("### cleanup files")
        for file in self.rmlist:
            self.log.info("### - " + file)
            os.remove(file)

    #
    # misc helper functions
    #

    def find_qemu_binary(self):
        qemu_path = [
            "/usr/local/bin/qemu-system-%s" % self.arch,
            "/usr/bin/qemu-system-%s" % self.arch,
        ]
        if self.arch == self.hostarch:
            qemu_path.insert(0, "/usr/bin/qemu-kvm")
            qemu_path.insert(0, "/usr/libexec/qemu-kvm")
        if not QEMU_BINARY is None:
            qemu_path.insert(0, QEMU_BINARY)
        for item in qemu_path:
            if os.path.isfile(item):
                return item;
        return None

    def find_ws(self, pattern, name):
        list = glob.glob(WORKSPACE + "/" + pattern)
        if len(list) == 0:
            self.cancel(name + " not found (" + pattern + ")")
        file = list[0]
        if not os.path.exists(file):
            self.cancel(name + " not found (" + pattern + ")")
        return file

    def find_fw(self, pattern, name):
        if not WORKSPACE is None:
            return self.find_ws(pattern, name)
        env = "edk2_" + name
        env = env.upper()
        self.log.info("### WORKSPACE is unset, checking %s ...", env)
        file = os.environ.get(env);
        if file is None:
            self.cancel("must set WORKSPACE or " + env)
        if not os.path.exists(file):
            self.cancel(name + " not found (" + pattern + ")")
        return file

    def pad_fw(self, src):
        dst = self.workdir + "/" + os.path.basename(src) + ".64m"
        self.log.info("### 64m pad  : %s -> %s" % (src, dst))
        subprocess.run([ 'dd', 'of=' + dst, 'if=/dev/zero', 'bs=1M', 'count=64', 'status=none'])
        subprocess.run([ 'dd', 'of=' + dst, 'if=' + src, 'conv=notrunc', 'status=none'])
        return dst

    def console_trace(self, name):
        while True:
            msg = self.rconsole.readline()
            self.lconsole.debug("%s: %s" % (name, msg.rstrip()))
            if '--[ end trace' in msg:
                break

    def console_wait(self, good, bad = None, errmsg = None):
        output = ""
        counter = 1
        while True:
            msg = self.rconsole.readline()
            self.lconsole.debug("%3d: %s" % (counter, msg.rstrip()))
            counter += 1
            if good in msg:
                break
            if not bad is None:
                if bad in msg:
                    if errmsg is None:
                        errmsg = "unexpected output (%s)" % bad
                    self.fail(errmsg)
            if 'Oops: ' in msg:
                self.console_trace("oops")
                self.fail("linux kernel oops")
            if 'WARNING: ' in msg:
                self.console_trace("warn")
                self.fail("linux kernel warn")
            if len(msg) == 0:
                if not self.vm.is_running():
                    self.fail("unexpected qemu exit %d" % self.vm.exitcode())
                else:
                    self.fail("unexpected console eof")
            if counter > 10000:
                self.fail("too much output")
            output += msg
        return output

    def console_wait_char(self, good):
        msg = ""
        counter = 1
        while True:
            char = self.rconsole.read(1)
            if char == '\n':
                if len(msg) != 0:
                    self.lconsole.debug("%3d: %s [line]" % (counter, msg))
                counter += 1
                msg = ""
            else:
                msg += char
            if good in msg:
                break
        if len(msg) != 0:
            self.lconsole.debug("%3d: %s [match]" % (counter, msg))

    # for linux
    def console_send(self, line = ""):
        self.log.info('out: "%s"', line)
        self.wconsole.write(line)
        self.wconsole.write('\n')
        self.wconsole.flush()

    # for uefi shell
    def console_send_crln(self, line = ""):
        self.log.info('out: "%s"', line)
        self.wconsole.write(line)
        self.wconsole.write('\r\n')
        self.wconsole.flush()

    def console_send_esc(self):
        self.log.info('out: ESC')
        self.wconsole.write('\x1b')
        self.wconsole.flush()

    def common_linux_shell_init(self):
        self.console_wait_char('#')
        self.console_send('PS1="\\n---\\u---\\n"')
        self.console_wait('---root---')
        self.console_send('TERM=dumb') # no colors please
        self.console_wait('---root---')
        self.console_send('pass=PASSED')
        self.console_wait('---root---')
        self.console_send('fail=FAILED')
        self.console_wait('---root---')

    def common_linux_buildroot_init(self):
        self.console_wait('Welcome to Buildroot')
        self.log.info("### buildroot login")
        self.console_wait_char('login:')
        self.console_send('root')
        self.common_linux_shell_init()

    def common_linux_distro_login(self):
        with open (PASSWD_FILE, "r") as file:
            password = file.read()
        self.console_wait('Permit User Sessions')
        self.console_wait_char('login:')
        self.console_send('root')
        self.console_wait_char('Password:')
        self.console_send(password)

    def common_linux_shutdown(self):
        self.console_send('halt')
        self.console_wait('System halted')

    def common_test_suspend(self):
        self.common_launch()
        self.common_linux_buildroot_init()

        count = 3
        for i in range(count):
            self.log.info("### suspend cycle %d/%d", i + 1, count)
            self.console_send('echo mem > /sys/power/state')
            self.console_wait('Suspending console')
            time.sleep(3)
            self.vm.command('system_wakeup')
            self.console_wait('---root---')
            time.sleep(3)

        self.log.info("### shutdown", i + 1, count)
        self.common_linux_shutdown()

    def common_add_tpm(self, tpm2 = False, dev = 'tpm-tis'):
        ctl = self.vm._sock_dir + "/" + self.vm._name + '-swtpm.sock'
        cmd = '/usr/bin/swtpm'
        cmd += ' socket'
        if tpm2:
            cmd += ' --tpm2'
        cmd += ' --tpmstate dir=' + self.workdir
        cmd += ' --ctrl type=unixio,path=' + ctl
        self.swtpm = avocado.utils.process.SubProcess(cmd)
        self.swtpm.start()
        while not os.path.exists(ctl):
            time.sleep(0.1)

        self.vm.add_args('-chardev', 'socket,id=tpm,path=' + ctl)
        self.vm.add_args('-tpmdev', 'emulator,id=tpm0,chardev=tpm')
        self.vm.add_args('-device', dev + ',tpmdev=tpm0')

    def common_linux_check_tpm(self, tpm2 = False):
        self.console_send('dmesg | grep -i tpm')
        self.console_wait('---root---')
        if tpm2:
            self.console_send('tpm2_selftest --verbose --full && echo $pass')
            self.console_wait('PASSED', 'ERROR')
            self.console_wait('---root---')

    def common_setup_logging(self):
        fh = logging.FileHandler(self.logdir + "/qemu.log")
        logging.getLogger('qemu').addHandler(fh)
        logging.getLogger('QMP').addHandler(fh)

    def common_prepare(self, arch, machine, device = None):
        self.arch = arch
        self.hostarch = os.uname()[4]

        binary = self.find_qemu_binary()
        self.log.info("### qemu     : %s", binary)

        if device is not None:
            devhelp = subprocess.run([ binary, '-device', 'help' ],
                                     stdout = subprocess.PIPE)
            devfind = 'name "%s"' % device
            if devhelp.stdout.decode().find(devfind) == -1:
                self.cancel('device %s not supported' % device)

        if self.nsenter is not None:
            self.log.info("### wrapper  : %s", self.nsenter)
            self.vm = QEMUMachine(binary, [], self.nsenter.split(' '))
        else:
            self.vm = QEMUMachine(binary)
        self.vm.set_machine(machine)
        self.vm.set_console()
        self.common_setup_logging()

        self.vm.add_args('-machine', 'accel=kvm:tcg')
        self.vm.add_args('-m', '1G')
        self.vm.add_args('-boot', 'menu=off')

        if self.arch == 'i386' or self.arch == 'x86_64':
            self.vm.add_args('-chardev', 'file,id=fw,path=%s/firmware.log' % self.logdir)
            self.vm.add_args('-device', 'isa-debugcon,iobase=0x402,chardev=fw')

        if self.arch == self.hostarch:
            self.vm.add_args('-cpu', 'host')
        elif self.arch == 'aarch64':
            self.vm.add_args('-cpu', 'cortex-a53')
        elif self.arch == 'arm':
            self.vm.add_args('-cpu', 'cortex-a15')

    def check_build(self, code, vars):
        if code is not None:
            codeinfo = subprocess.run([ 'python3', 'tools/dumpfv.py', code ],
                                      stdout = subprocess.PIPE)
            self.needs_smm  = (codeinfo.stdout.decode().find('SmmLockBox') != -1)
            self.has_tpm1   = (codeinfo.stdout.decode().find('TcgDxe') != -1)
            self.has_tpm2   = (codeinfo.stdout.decode().find('Tcg2Dxe') != -1)
        if vars is not None:
            varsinfo = subprocess.run([ 'python3', 'tools/dumpfv.py', vars ],
                                      stdout = subprocess.PIPE)
            self.sb_enabled = (varsinfo.stdout.decode().find('SecureBootEnable') != -1)

    def common_add_rom(self, image):
        rom = self.find_fw(image, "rom")
        self.log.info("### rom image: %s", rom)
        self.vm.add_args('-bios', rom)
        self.check_build(rom, None)
        if self.needs_smm:
            self.cancel("smm build requires flash")

    def common_add_flash(self, image_code, image_vars, store_vars = None):
        code = self.find_fw(image_code, "code")
        tmpl = self.find_fw(image_vars, "vars")

        if self.arch == 'aarch64' or self.arch == 'arm':
            # pad arm images to 64m size if needed
            if os.path.getsize(code) != 64 * 1024 * 1024:
                code = self.pad_fw(code)
                self.rmlist.append(code)
            if os.path.getsize(tmpl) != 64 * 1024 * 1024:
                tmpl = self.pad_fw(tmpl)
                self.rmlist.append(tmpl)

        copy = "%s/vars.raw" % self.workdir
        self.rmlist.append(copy)
        if not store_vars is None:
            self.cplist[copy] = store_vars
        shutil.copyfile(tmpl, copy)

        self.log.info("### code     : %s", code)
        self.log.info("### vars tmpl: %s", tmpl)
        self.log.info("### vars copy: %s", copy)

        self.check_build(code, copy)
        if self.needs_smm:
            self.log.info("### needs smm: yes")
            self.vm.add_args('-machine', 'smm=on')
            if self.vm._machine != 'q35':
                self.cancel("smm build requires q35")

        self.vm.add_args('-blockdev', 'node-name=code,driver=file,filename=%s,read-only=on' % code)
        self.vm.add_args('-blockdev', 'node-name=vars,driver=file,filename=%s' % copy)
        self.vm.add_args('-machine', 'pflash0=code')
        self.vm.add_args('-machine', 'pflash1=vars')

    def common_enable_iommu(self):
        if self.arch == 'x86_64':
            self.vm.add_args('-machine', 'kernel-irqchip=split')
            self.vm.add_args('-device', 'intel-iommu,intremap=on,device-iotlb=on')
            self.vm.add_args('-global', 'virtio-pci.iommu_platform=on')
        if self.arch == 'aarch64':
            self.vm.add_args('-machine', 'iommu=smmuv3')
            self.vm.add_args('-global', 'virtio-pci.iommu_platform=on')

    def common_launch(self):
        self.log.info("### launch guest")
        try:
            self.vm.launch()
        except:
            self.fail("qemu failed")
        self.rconsole = self.vm.console_socket.makefile('r', encoding = 'latin1')
        self.wconsole = self.vm.console_socket.makefile('w', encoding = 'latin1')
        self.lconsole = logging.getLogger('console')
        fh = logging.FileHandler(self.logdir + "/sercon.log")
        self.lconsole.addHandler(fh)

    def common_boot_kernel(self):
        kernel = "unknown"
        serial = "unknown"

        # buildroot kernels
        if self.arch == 'aarch64':
            kernel = 'buildroot/initrd-aarch64/Image'
            serial = 'ttyAMA0'
        elif self.arch == 'arm':
            kernel = 'buildroot/initrd-arm/zImage'
            serial = 'ttyAMA0'
        elif self.arch == 'i386':
            kernel = 'buildroot/initrd-i386/bzImage'
            serial = 'ttyS0'
        elif self.arch == 'x86_64':
            kernel = 'buildroot/initrd-x86_64/bzImage'
            serial = 'ttyS0'

        # try host kernel instead
        if not os.path.exists(kernel) and self.arch == self.hostarch:
            kernel = '/boot/vmlinuz-' + os.uname()[2]

        if not os.path.exists(kernel):
            self.cancel("kernel not found (" + kernel + ")")

        self.log.info("### kernel   : %s", kernel)
        self.vm.add_args('-no-reboot')
        self.vm.add_args('-kernel', kernel)
        self.vm.add_args('-append', 'panic=1 console=' + serial)

        self.common_launch()
        self.console_wait("Rebooting in 1 sec")

    def common_add_qcow2_disk(self, file, name = "disk"):
        if not os.path.exists(file):
            self.fail("qcow2 not found")
        copy = self.workdir + "/" + name + ".qcow2"
        self.log.info("### disk img : %s", file)
        self.log.info("### disk cow : %s", copy)
        cmdline = "qemu-img create -f qcow2 -F qcow2"
        cmdline += " -b " + os.path.abspath(file) + " " + copy
        avocado.utils.process.run(cmdline)
        self.rmlist.append(copy)
        blkdev = 'node-name=' + name
        blkdev += ',driver=qcow2'
        blkdev += ',file.driver=file'
        blkdev += ',file.filename=' + copy
        self.vm.add_args('-blockdev', blkdev)
        
    def common_add_raw_iso(self, file, name = "iso"):
        if not os.path.exists(file):
            self.fail("iso not found")
        self.log.info("### iso img  : %s", file)
        blkdev = 'node-name=' + name
        blkdev += ',driver=file'
        blkdev += ',filename=' + file
        blkdev += ',read-only=on'
        self.vm.add_args('-blockdev', blkdev)
        
    def common_add_user_net(self, name = "net", tftp = None, bootfile = None):
        netdev = "user,id=" + name
        if not tftp is None:
            netdev += ",tftp=" + tftp
        if not bootfile is None:
            netdev += ",bootfile=" + bootfile
        self.vm.add_args('-netdev', netdev)
        
    def common_add_disk(self, kind, name = "disk", bootindex = 1):
        device = kind
        device += ',bootindex=' + str(bootindex)
        device += ',drive=' + name
        self.vm.add_args('-device', device)
        
    def common_add_pcie_port(self):
        name = 'port' + str(self.pcie)
        device = 'pcie-root-port'
        device += ',id=' + name
        device += ',bus=pcie.0'
        device += ',addr=%02x.%x' % (8 + (self.pcie >> 3), (self.pcie & 7))
        device += ',chassis=' + str(self.pcie)
        device += ',multifunction=on'
        self.vm.add_args('-device', device)
        self.pcie += 1
        return name
        
    def common_add_virtio_blk_pci(self, name = "disk"):
        self.common_add_disk('virtio-blk-pci', name)
        
    def common_add_virtio_blk_mmio(self, name = "disk"):
        self.common_add_disk('virtio-blk-device', name)
        
    def common_add_virtio_blk_pcie(self, name = "disk"):
        port = self.common_add_pcie_port()
        device = 'virtio-blk-pci'
        device += ',bus=' + port
        device += ',bootindex=1'
        device += ',drive=' + name
        self.vm.add_args('-device', device)
        
    def common_add_virtio_scsi_pci(self):
        self.vm.add_args('-device', 'virtio-scsi-pci')
        
    def common_add_virtio_scsi_mmio(self):
        self.vm.add_args('-device', 'virtio-scsi-device')
        
    def common_add_virtio_scsi_pcie(self):
        port = self.common_add_pcie_port()
        device = 'virtio-scsi-pci'
        device += ',bus=' + port
        self.vm.add_args('-device', device)
        
    def common_add_nvme_pcie(self):
        port = self.common_add_pcie_port()
        device = 'nvme'
        device += ',bus=' + port
        device += ',serial=123456789'
        self.vm.add_args('-device', device)
        
    def common_add_xhci_pcie(self):
        port = self.common_add_pcie_port()
        device = 'qemu-xhci'
        device += ',bus=' + port
        self.vm.add_args('-device', device)
        
    def common_add_nic(self, kind, netdev = "net", bootindex = 1):
        device = kind
        device += ',bootindex=' + str(bootindex)
        device += ',netdev=' + netdev
        self.vm.add_args('-device', device)
        
    def common_add_nic_pcie(self, kind, netdev = "net", bootindex = 1):
        port = self.common_add_pcie_port()
        device = kind
        device += ',bootindex=' + str(bootindex)
        device += ',netdev=' + netdev
        device += ',bus=' + port
        self.vm.add_args('-device', device)
        
    def common_boot_disk(self):
        self.common_launch()
        self.common_linux_buildroot_init()

        self.console_send('lspci -vnn')
        self.console_wait('---root---')

        self.common_linux_shutdown()
