#
# ovmf test cases, suspend, ia32x64
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestCore import Edk2TestCore

@avocado.skipUnless(os.path.exists('buildroot/efidisk-x86_64/efidisk.qcow2'), 'no disk image')
class TestDisk(Edk2TestCore):

    timeout = 60

    def common_prepare_x64(self, machine):
        self.common_prepare('x86_64', machine)
        self.common_add_flash('Build/Ovmf3264/DEBUG_*/FV/OVMF_CODE.fd',
                              'Build/Ovmf3264/DEBUG_*/FV/OVMF_VARS.fd')
        if self.sb_enabled:
            self.cancel("sb is enabled")
        self.common_add_qcow2_disk('buildroot/efidisk-x86_64/efidisk.qcow2')

    def test_ovmf_q35_suspend(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:suspend
        """
        self.common_prepare_x64('q35')
        self.vm.add_args('-global', 'ICH9-LPC.disable_s3=0')
        self.common_add_virtio_blk_pcie()
        self.common_test_suspend()
