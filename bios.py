#
# seabios smoke test
# makes sure we have a working qemu install
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestCore import Edk2TestCore

class TestBIOS(Edk2TestCore):

    timeout = 20

    def test_x64_pc(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:pc
        """
        self.common_prepare('x86_64', 'pc')
        self.common_boot_kernel()

    def test_x64_q35(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        """
        self.common_prepare('x86_64', 'q35')
        self.common_boot_kernel()

    def test_x64_microvm(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:microvm
        """
        self.common_prepare('x86_64', 'microvm,rtc=on')
        self.common_boot_kernel()

    def test_ia32_pc(self):
        """
        :avocado: tags=arch:i386
        :avocado: tags=machine:pc
        """
        self.common_prepare('i386', 'pc')
        self.common_boot_kernel()

    def test_aa64_virt(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        """
        self.common_prepare('aarch64', 'virt')
        self.common_boot_kernel()

    def test_arm_virt(self):
        """
        :avocado: tags=arch:arm
        :avocado: tags=machine:virt
        """
        self.common_prepare('arm', 'virt')
        self.common_boot_kernel()
