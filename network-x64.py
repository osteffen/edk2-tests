#
# ovmf test cases, boot from network, x64
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestCore import Edk2TestCore

@avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/EFI/BOOT/grub.efi'),
                    'no grub image')
class TestNet(Edk2TestCore):

    timeout = 60

    def common_prepare_x64_net(self, machine):
        self.common_prepare('x86_64', machine)
        self.common_add_flash('Build/OvmfX64/DEBUG_*/FV/OVMF_CODE.fd',
                              'Build/OvmfX64/DEBUG_*/FV/OVMF_VARS.fd')
        if self.sb_enabled:
            self.cancel("sb is enabled")
        self.common_add_user_net('boot', 'buildroot/initrd-x86_64',
                                 '/EFI/BOOT/grub.efi')
        
    def test_ovmf_pc_virtio_net_pci(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:pc
        :avocado: tags=network:virtio-net
        """
        self.common_prepare_x64_net('pc')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_boot_disk()

    def test_ovmf_pc_e1000_pci(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:pc
        :avocado: tags=network:e1000
        """
        self.common_prepare_x64_net('pc')
        self.common_add_nic('e1000', 'boot')
        self.common_boot_disk()

    def test_ovmf_q35_virtio_net_pcie(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        :avocado: tags=io:pcie
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic_pcie('virtio-net-pci', 'boot')
        self.common_boot_disk()

    def test_ovmf_q35_e1000e_pcie(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:e1000e
        :avocado: tags=io:pcie
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic_pcie('e1000e', 'boot')
        self.common_boot_disk()
