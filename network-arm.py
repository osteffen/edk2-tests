#
# armvirt test cases, boot from network
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestCore import Edk2TestCore

@avocado.skipUnless(os.path.exists('buildroot/initrd-arm/EFI/BOOT/grub.efi'),
                    'no grub image')
class TestNet(Edk2TestCore):

    timeout = 60

    def common_prepare_arm_net(self):
        self.common_prepare('arm', 'virt')
        self.common_add_flash('Build/ArmVirtQemu-ARM/DEBUG_*/FV/QEMU_EFI.fd',
                              'Build/ArmVirtQemu-ARM/DEBUG_*/FV/QEMU_VARS.fd')
        self.common_add_user_net('boot', 'buildroot/initrd-arm',
                                 '/EFI/BOOT/grub.efi')
        
    def test_arm_virtio_net_pci(self):
        """
        :avocado: tags=arch:arm
        :avocado: tags=machine:virt
        :avocado: tags=network:virtio-net
        """
        self.common_prepare_arm_net()
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_boot_disk()
