#
# ovmf test cases, boot from disk, x64
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestCore import Edk2TestCore

@avocado.skipUnless(os.path.exists('buildroot/efidisk-x86_64/efidisk.qcow2'), 'no disk image')
class TestDisk(Edk2TestCore):

    timeout = 60

    def common_prepare_x64_disk(self, machine, device = None):
        self.common_prepare('x86_64', machine, device)
        self.common_add_flash('Build/OvmfX64/DEBUG_*/FV/OVMF_CODE.fd',
                              'Build/OvmfX64/DEBUG_*/FV/OVMF_VARS.fd')
        if self.sb_enabled:
            self.cancel("sb is enabled")
        self.common_add_qcow2_disk('buildroot/efidisk-x86_64/efidisk.qcow2')

        
    def test_ovmf_pc_ide(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:pc
        :avocado: tags=storage:ide
        """
        self.common_prepare_x64_disk('pc')
        self.common_add_disk('ide-hd')
        self.common_boot_disk()

    def test_ovmf_pc_virtio_blk_pci(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:pc
        :avocado: tags=storage:virtio-blk
        """
        self.common_prepare_x64_disk('pc')
        self.common_add_virtio_blk_pci()
        self.common_boot_disk()

    def test_ovmf_pc_virtio_scsi_pci(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:pc
        :avocado: tags=storage:virtio-scsi
        """
        self.common_prepare_x64_disk('pc')
        self.common_add_virtio_scsi_pci()
        self.common_add_disk('scsi-hd')
        self.common_boot_disk()

        
    def test_ovmf_q35_sata(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=storage:sata
        """
        self.common_prepare_x64_disk('q35')
        self.common_add_disk('ide-hd')
        self.common_boot_disk()

    def test_ovmf_q35_virtio_blk_pcie(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=storage:virtio-blk
        :avocado: tags=io:pcie
        """
        self.common_prepare_x64_disk('q35')
        self.common_add_virtio_blk_pcie()
        self.common_boot_disk()

    def test_ovmf_q35_virtio_blk_pcie_iommu(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=storage:virtio-blk
        :avocado: tags=io:pcie
        :avocado: tags=io:iommu
        """
        self.common_prepare_x64_disk('q35')
        self.common_enable_iommu()
        self.common_add_virtio_blk_pcie()
        self.common_boot_disk()

    def test_ovmf_q35_virtio_scsi_pcie(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=storage:virtio-scsi
        :avocado: tags=io:pcie
        """
        self.common_prepare_x64_disk('q35')
        self.common_add_virtio_scsi_pcie()
        self.common_add_disk('scsi-hd')
        self.common_boot_disk()

    def test_ovmf_q35_virtio_scsi_pcie_iommu(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=storage:virtio-scsi
        :avocado: tags=io:pcie
        :avocado: tags=io:iommu
        """
        self.common_prepare_x64_disk('q35')
        self.common_enable_iommu()
        self.common_add_virtio_scsi_pcie()
        self.common_add_disk('scsi-hd')
        self.common_boot_disk()

    def test_ovmf_q35_nvme_pcie(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=storage:nvme
        :avocado: tags=io:pcie
        """
        self.common_prepare_x64_disk('q35', 'nvme-ns')
        self.common_add_nvme_pcie()
        self.common_add_disk('nvme-ns')
        self.common_boot_disk()

    def test_ovmf_q35_usb_storage(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=storage:usb
        """
        self.common_prepare_x64_disk('q35', 'usb-storage')
        self.common_add_xhci_pcie()
        self.common_add_disk('usb-storage')
        self.common_boot_disk()

    def test_ovmf_microvm_virtio_blk_mmio(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:microvm
        :avocado: tags=storage:virtio-blk
        """
        self.common_prepare('x86_64', 'microvm,rtc=on')
        self.common_add_rom('Build/MicrovmX64/DEBUG_*/FV/MICROVM.fd')
        self.common_add_qcow2_disk('buildroot/efidisk-x86_64/efidisk.qcow2')

        self.common_add_virtio_blk_mmio()
        self.common_boot_disk()

    def test_ovmf_microvm_virtio_scsi_mmio(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:microvm
        :avocado: tags=storage:virtio-scsi
        """
        self.common_prepare('x86_64', 'microvm,rtc=on')
        self.common_add_rom('Build/MicrovmX64/DEBUG_*/FV/MICROVM.fd')
        self.common_add_qcow2_disk('buildroot/efidisk-x86_64/efidisk.qcow2')

        self.common_add_virtio_scsi_mmio()
        self.common_add_disk('scsi-hd')
        self.common_boot_disk()

    @avocado.skip("not merget yet")
    def test_ovmf_microvm_virtio_blk_pcie(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:microvm
        :avocado: tags=storage:virtio-blk
        :avocado: tags=io:pcie
        """
        self.common_prepare('x86_64', 'microvm,rtc=on,pcie=on')
        self.common_add_rom('Build/MicrovmX64/DEBUG_*/FV/MICROVM.fd')
        self.common_add_qcow2_disk('buildroot/efidisk-x86_64/efidisk.qcow2')

        self.common_add_virtio_blk_pcie()
        self.common_boot_disk()

    @avocado.skip("not merget yet")
    def test_ovmf_microvm_virtio_scsi_pcie(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:microvm
        :avocado: tags=storage:virtio-scsi
        :avocado: tags=io:pcie
        """
        self.common_prepare('x86_64', 'microvm,rtc=on,pcie=on')
        self.common_add_rom('Build/MicrovmX64/DEBUG_*/FV/MICROVM.fd')
        self.common_add_qcow2_disk('buildroot/efidisk-x86_64/efidisk.qcow2')

        self.common_add_virtio_scsi_pcie()
        self.common_add_disk('scsi-hd')
        self.common_boot_disk()
