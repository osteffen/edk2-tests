# minimal config
rootpw --plaintext root
firstboot --disable
reboot

clearpart --all --initlabel --disklabel=gpt --drives=sda
bootloader --append="console=ttyS0" --boot-drive=sda
autopart --type=plain

%packages

# minimal package list
@core
-dracut-config-rescue
dracut-config-generic

# needed for tests
pciutils

%end
