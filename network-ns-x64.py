#
# ovmf test cases, using network namespaces, x64
#

import os
import time
import logging
import subprocess

# avocado
import avocado
import avocado.utils.process

# local
from Edk2TestCore import Edk2TestCore

# config
dev = "net0"
dn4 = "boot.v4.net.work"
dn6 = "boot.v6.net.work"
ip4 = "192.168.42.1"
ip6 = "fd42::1"

@avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/EFI/BOOT/grub.efi'),
                    'no grub image')
class TestNetNS(Edk2TestCore):

    timeout = 60
    unshare = None
    pcapture = None
    radvd = None
    dnsmasq = None
    webserver = None

    def setUp(self):
        Edk2TestCore.setUp(self)
        cmdline = 'unshare -n -U -r'
        helptext = subprocess.run([ 'unshare', '--help' ], stdout = subprocess.PIPE)
        if helptext.stdout.decode().find('--map-group') != -1:
            cmdline += ' --map-group wireshark'
        cmdline += ' sleep %d' % self.timeout
        self.log.info('### namespace')
        self.unshare = avocado.utils.process.SubProcess(cmdline)
        self.unshare.start()
        self.nsenter = '/usr/bin/nsenter -n -U --preserve-credentials -t %d' % self.unshare.get_pid()
        time.sleep(1)  # FIXME: wait for unshare init namespace

        self.log.info('### network setup')
        cmdline = 'tools/netinit.sh'
        if not self.nsenter == None:
            cmdline = self.nsenter + ' ' + cmdline
        try:
            avocado.utils.process.run(cmdline)
        except:
            self.cancel("netinit failed")

    def tearDown(self):
        self.log.info('### stop processes')
        if self.webserver is not None:
            self.webserver.stop()
        if self.dnsmasq is not None:
            self.dnsmasq.stop()
        if self.radvd is not None:
            self.radvd.stop()
        if self.pcapture is not None:
            self.pcapture.stop()
        if self.unshare is not None:
            self.unshare.stop()
        Edk2TestCore.tearDown(self)

    def make_readable(self, filename):
        count = 0
        while not os.path.exists(filename):
            count += 1
            if count > 10:
                return
            time.sleep(0.1)
        os.chmod(filename, 0o644)

    def common_dnsmasq(self, append = None):
        self.log.info('### start dnsmasq')
        logfile = self.logdir + "/dnsmasq.log"
        pidfile = self.workdir + "/dnsmasq.pid"
        cmdline = 'tools/dnsmasq.sh'
        cmdline += ' --log-facility=' + logfile
        cmdline += ' --pid-file=' + pidfile
        if not append == None:
            cmdline += ' ' + append
        if not self.nsenter == None:
            cmdline = self.nsenter + ' ' + cmdline
        self.dnsmasq = avocado.utils.process.SubProcess(cmdline,
                                                        verbose = False)
        self.dnsmasq.start()
        self.make_readable(logfile)

    def common_radvd(self):
        self.log.info('### start radvd')
        logfile = self.logdir + "/radvd.log"
        cmdline = 'tools/radvd.sh'
        cmdline += ' --logmethod logfile'
        cmdline += ' --logfile ' + logfile
        if not self.nsenter == None:
            cmdline = self.nsenter + ' ' + cmdline
        self.radvd = avocado.utils.process.SubProcess(cmdline)
        self.radvd.start()
        self.make_readable(logfile)

    def common_http(self, dir, addr):
        self.log.info('### start webserver')
        cmdline = 'python3 tools/webserver.py'
        cmdline += ' --directory=' + dir
        cmdline += ' --bind=' + addr
        if not self.nsenter == None:
            cmdline = self.nsenter + ' ' + cmdline
        self.webserver = avocado.utils.process.SubProcess(cmdline)
        self.webserver.start()

    def common_https(self, dir, addr):
        self.log.info('### start secure webserver')
        mkcert = "tools/makecert.sh " + self.workdir
        avocado.utils.process.run(mkcert, verbose = True)

        self.vm.add_args('-object', 'tls-cipher-suites,id=tls-cipher0,priority=@SYSTEM')
        self.vm.add_args('-fw_cfg', 'name=etc/edk2/https/ciphers,gen_id=tls-cipher0')
        self.vm.add_args('-fw_cfg', 'name=etc/edk2/https/cacerts,file=' + self.workdir + '/ca.db')

        cmdline = 'python3 tools/webserver.py'
        cmdline += ' --directory=' + dir
        cmdline += ' --bind=' + addr
        cmdline += ' --tls'
        cmdline += ' --cert=' + self.workdir + '/server-chain.cert'
        cmdline += ' --key=' + self.workdir + '/server.key'
        if not self.nsenter == None:
            cmdline = self.nsenter + ' ' + cmdline
        self.webserver = avocado.utils.process.SubProcess(cmdline)
        self.webserver.start()

    def common_pcapture(self):
        if not os.path.exists("/usr/bin/dumpcap"):
            self.log.info('### skip packet capture (dumpcap not installed)')
            return
        self.log.info('### start packet capture')
        tapfile = self.outputdir + '/' + dev + '.pcap'
        cmdline = 'dumpcap'
        cmdline += ' -i ' + dev
        cmdline += ' -w ' + tapfile
        if not self.nsenter == None:
            cmdline = self.nsenter + ' ' + cmdline
        self.pcapture = avocado.utils.process.SubProcess(cmdline)
        self.pcapture.start()
        self.make_readable(tapfile)

    def common_add_tap_net(self, name = 'net', tap = 'tap0'):
        netdev = 'tap,id=' + name
        netdev += ',ifname=' + tap
        netdev += ',script=/bin/true'
        netdev += ',downscript=/bin/true'
        self.vm.add_args('-netdev', netdev)
        
    def common_prepare_x64_net(self, machine):
        self.common_prepare('x86_64', machine)
        self.common_add_flash('Build/OvmfX64/DEBUG_*/FV/OVMF_CODE.fd',
                              'Build/OvmfX64/DEBUG_*/FV/OVMF_VARS.fd')
        if self.sb_enabled:
            self.cancel("sb is enabled")
        self.common_add_tap_net('boot')
        
    def test_ovmf_q35_tftp4(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        self.common_dnsmasq('--dhcp-boot=/EFI/BOOT/grubx64.efi'
                            + ' --enable-tftp'
                            + ' --tftp-root=' + os.getcwd() + '/buildroot/initrd-x86_64')

        self.common_boot_disk()

    def test_ovmf_q35_tftp6(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        url = 'tftp://[' + ip6 + ']/EFI/BOOT/grubx64.efi'
        self.common_radvd()
        self.common_dnsmasq('--dhcp-boot=/404' # make PXEv4 fail fast
                            + ' --dhcp-option=option6:bootfile-url,' + url
                            + ' --enable-tftp'
                            + ' --tftp-root=' + os.getcwd() + '/buildroot/initrd-x86_64')

        self.common_boot_disk()

    @avocado.skip("broken") # edk2 or grub sending http/1.1 req to http/1.0 server
    def test_ovmf_q35_http4_dir(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        url = 'http://' + dn4 + ':8080/EFI/BOOT/grubx64.efi'
        self.common_dnsmasq('--dhcp-boot=' + url
                            + ' --dhcp-option-force=60,HTTPClient')
        self.common_http(os.getcwd() + '/buildroot/initrd-x86_64', ip4)

        self.common_boot_disk()

    @avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/boot.iso'), 'no iso image')
    def test_ovmf_q35_http4_iso(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        url = 'http://' + dn4 + ':8080/boot.iso'
        self.common_dnsmasq('--dhcp-boot=' + url
                            + ' --dhcp-option-force=60,HTTPClient')
        self.common_http(os.getcwd() + '/buildroot/initrd-x86_64', ip4)

        self.common_boot_disk()

    @avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/boot.iso'), 'no iso image')
    def test_ovmf_q35_https4_iso(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        url = 'https://' + dn4 + ':8080/boot.iso'
        self.common_dnsmasq('--dhcp-boot=' + url
                            + ' --dhcp-option-force=60,HTTPClient')
        self.common_https(os.getcwd() + '/buildroot/initrd-x86_64', ip4)

        time.sleep(1)
        cmd = 'echo -ne "GET / HTTP/1.0\\r\\n\\r\\n" | '
        cmd += self.nsenter + ' openssl s_client'
        cmd += ' -CAfile ' + self.workdir + '/ca.cert'
        cmd += ' -connect ' + dn4 + ':8080'
        avocado.utils.process.run(cmd, shell = True)

        self.common_boot_disk()

    @avocado.skip("wip")
    def test_ovmf_q35_http6_dir(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        url = 'http://' + dn6 + ':8080/EFI/BOOT/grubx64.efi'
        self.common_radvd()
        self.common_dnsmasq('--dhcp-boot=' + url
                            + ' --dhcp-option=option6:bootfile-url,' + url
                            + ' --dhcp-option-force=60,HTTPClient')
        self.common_http(os.getcwd() + '/buildroot/initrd-x86_64', '::')

        self.common_boot_disk()

    @avocado.skip("wip")
    @avocado.skipUnless(os.path.exists('buildroot/initrd-x86_64/boot.iso'), 'no iso image')
    def test_ovmf_q35_http6_iso(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=network:virtio-net
        """
        self.common_prepare_x64_net('q35')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_pcapture()

        url = 'http://' + dn6 + ':8080/boot.iso'
        self.common_radvd()
        self.common_dnsmasq('--dhcp-boot=' + url
                            + ' --dhcp-option=option6:bootfile-url,' + url
                            + ' --dhcp-option-force=60,HTTPClient')
        self.common_http(os.getcwd() + '/buildroot/initrd-x86_64', '::')

        self.common_boot_disk()
