#
# ovmf test cases, secure boot, x64
#

import os
import time
import logging
import subprocess

# avocado
import avocado
import avocado.utils.process

# local
from Edk2TestCore import Edk2TestCore, FEDORA_X64, STREAM8_X64, STREAM9_X64, RHEL7_X64, RHEL8_X64

class TestSB(Edk2TestCore):

    timeout = 60

    def common_secure_boot_enroll_1(self):
        self.common_add_raw_iso("/usr/share/edk2/ovmf/UefiShell.iso")
        self.common_add_disk('scsi-cd', 'iso', 1)
        self.vm.add_args('-smbios', 'type=11,path=RedHatSecureBootPkKek1.oemstr')

    def common_secure_boot_enroll_2(self):
        self.console_wait_char('Press')
        self.console_send_esc()
        self.console_wait_char('Shell>')
        self.console_send_crln('fs0:enrolldefaultkeys.efi')
        self.console_wait('success', 'error', 'enroll failed')
        self.console_wait_char('Shell>')

    def common_linux_check_secure_boot(self):
        self.console_send('bootctl')
        self.console_wait('Secure Boot: enabled',
                          '---root---',
                          'secure boot disabled')
        self.console_wait('---root---')

    def common_secure_boot_distro(self, disk):
        self.common_prepare('x86_64', 'q35')
        self.common_add_flash('Build/Ovmf3264/DEBUG_*/FV/OVMF_CODE.fd',
                              'Build/Ovmf3264/DEBUG_*/FV/OVMF_VARS.fd')
        if not self.needs_smm:
            self.cancel("sb not supported")
        self.common_add_virtio_scsi_pcie()
        self.common_secure_boot_enroll_1()
        self.common_add_qcow2_disk(disk)
        self.common_add_disk('scsi-hd', 'disk', 2)
        self.common_launch()

        if not self.sb_enabled:
            self.common_secure_boot_enroll_2()
            self.console_send_crln('reset')

        self.common_linux_distro_login()
        self.common_linux_shell_init()
        self.common_linux_check_secure_boot()
        self.common_linux_shutdown()

    def test_ovmf_secure_boot_enroll(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:sb
        """
        self.common_prepare('x86_64', 'q35')
        self.common_add_flash('Build/Ovmf3264/DEBUG_*/FV/OVMF_CODE.fd',
                              'Build/Ovmf3264/DEBUG_*/FV/OVMF_VARS.fd',
                              self.outputdir + '/vars.sb.fd')
        if self.sb_enabled:
            self.cancel("sb is enabled")
        if not self.needs_smm:
            self.cancel("sb not supported")
        self.common_add_virtio_scsi_pcie()
        self.common_secure_boot_enroll_1()
        self.common_launch()

        self.common_secure_boot_enroll_2()
        self.console_send_crln('halt')

    @avocado.skipUnless(os.path.exists(FEDORA_X64), 'no fedora image')
    def test_ovmf_secure_boot_fedora(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:sb
        """
        self.common_secure_boot_distro(FEDORA_X64)

    @avocado.skipUnless(os.path.exists(STREAM8_X64), 'no centos stream 8 image')
    def test_ovmf_secure_boot_stream8(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:sb
        """
        self.common_secure_boot_distro(STREAM8_X64)

    @avocado.skip('uses beta keys')
    @avocado.skipUnless(os.path.exists(STREAM9_X64), 'no centos stream 9 image')
    def test_ovmf_secure_boot_stream9(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:sb
        """
        self.common_secure_boot_distro(STREAM9_X64)

    @avocado.skipUnless(os.path.exists(RHEL7_X64), 'no rhel 7 image')
    def test_ovmf_secure_boot_rhel7(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:sb
        """
        self.common_secure_boot_distro(RHEL7_X64)

    @avocado.skipUnless(os.path.exists(RHEL8_X64), 'no rhel 8 image')
    def test_ovmf_secure_boot_rhel8(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:sb
        """
        self.common_secure_boot_distro(RHEL8_X64)
