#
# armvirt test cases, boot from network
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestCore import Edk2TestCore

@avocado.skipUnless(os.path.exists('buildroot/initrd-aarch64/EFI/BOOT/grub.efi'),
                    'no grub image')
class TestNet(Edk2TestCore):

    timeout = 60

    def common_prepare_aa64_net(self):
        self.common_prepare('aarch64', 'virt')
        self.common_add_flash('Build/ArmVirtQemu-AARCH64/DEBUG_*/FV/QEMU_EFI.fd',
                              'Build/ArmVirtQemu-AARCH64/DEBUG_*/FV/QEMU_VARS.fd')
        self.common_add_user_net('boot', 'buildroot/initrd-aarch64',
                                 '/EFI/BOOT/grub.efi')
        
    def test_aa64_virtio_net_pci(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=network:virtio-net
        """
        self.common_prepare_aa64_net()
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_boot_disk()

    def test_aa64_virtio_net_pcie(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=network:virtio-net
        :avocado: tags=io:pcie
        """
        self.common_prepare_aa64_net()
        self.common_add_nic_pcie('virtio-net-pci', 'boot')
        self.common_boot_disk()

