#
# armvirt test cases, boot from disk
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestCore import Edk2TestCore

@avocado.skipUnless(os.path.exists('buildroot/efidisk-arm/efidisk.qcow2'), 'no disk image')
class TestDisk(Edk2TestCore):

    timeout = 60

    def common_prepare_aa64_disk(self):
        self.common_prepare('arm', 'virt')
        self.common_add_flash('Build/ArmVirtQemu-ARM/DEBUG_*/FV/QEMU_EFI.fd',
                              'Build/ArmVirtQemu-ARM/DEBUG_*/FV/QEMU_VARS.fd')
        self.common_add_qcow2_disk('buildroot/efidisk-arm/efidisk.qcow2')

        
    def test_arm_virtio_blk_pci(self):
        """
        :avocado: tags=arch:arm
        :avocado: tags=machine:virt
        :avocado: tags=storage:virtio-blk
        """
        self.common_prepare_aa64_disk()
        self.common_add_virtio_blk_pci()
        self.common_boot_disk()

    def test_arm_virtio_blk_mmio_095(self):
        """
        :avocado: tags=arch:arm
        :avocado: tags=machine:virt
        :avocado: tags=storage:virtio-blk
        :avocado: tags=io:mmio
        """
        self.common_prepare_aa64_disk()
        self.vm.add_args('-global', 'virtio-mmio.force-legacy=true')
        self.common_add_virtio_blk_mmio()
        self.common_boot_disk()

    def test_arm_virtio_blk_mmio_10(self):
        """
        :avocado: tags=arch:arm
        :avocado: tags=machine:virt
        :avocado: tags=storage:virtio-blk
        :avocado: tags=io:mmio
        """
        self.common_prepare_aa64_disk()
        self.vm.add_args('-global', 'virtio-mmio.force-legacy=false')
        self.common_add_virtio_blk_mmio()
        self.common_boot_disk()
