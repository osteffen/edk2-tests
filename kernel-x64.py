#
# ovmf test cases using direct kernel boot.
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestCore import Edk2TestCore

class TestKernel(Edk2TestCore):

    timeout = 20

    def test_ovmf_x64_pc_rom(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:pc
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'pc')
        self.common_add_rom('Build/OvmfX64/DEBUG_*/FV/OVMF.fd')
        self.common_boot_kernel()

    def test_ovmf_x64_pc_flash(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:pc
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'pc')
        self.common_add_flash('Build/OvmfX64/DEBUG_*/FV/OVMF_CODE.fd',
                              'Build/OvmfX64/DEBUG_*/FV/OVMF_VARS.fd')
        self.common_boot_kernel()

    def test_ovmf_x64_q35_rom(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'q35')
        self.common_add_rom('Build/OvmfX64/DEBUG_*/FV/OVMF.fd')
        self.common_boot_kernel()

    def test_ovmf_x64_q35_flash(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'q35')
        self.common_add_flash('Build/OvmfX64/DEBUG_*/FV/OVMF_CODE.fd',
                              'Build/OvmfX64/DEBUG_*/FV/OVMF_VARS.fd')
        self.common_boot_kernel()

    def test_ovmf_x64_microvm_rom(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:microvm
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'microvm,rtc=on')
        self.common_add_rom('Build/MicrovmX64/DEBUG_*/FV/MICROVM.fd')
        self.common_boot_kernel()

    @avocado.skip('broken')
    def test_ovmf_x64_q35_amdsev(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('x86_64', 'q35')
        self.common_add_rom('Build/AmdSev/DEBUG_*/FV/OVMF.fd')
        self.common_boot_kernel()
