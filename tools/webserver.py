#!/usr/bin/python
import os
import ssl
import socket
import optparse
import http.server

class HTTPServerV6(http.server.HTTPServer):
    address_family = socket.AF_INET6

parser = optparse.OptionParser()
parser.add_option('-d', '--directory', dest = 'directory', type = 'string', default = '.',
                  help = 'server files from DIR', metavar = 'DIR')
parser.add_option('-p', '--port', dest = 'port', type = 'int', default = 8080,
                  help = 'bind to tcp port PORT', metavar = 'PORT')
parser.add_option('-b', '--bind', dest = 'bind', type = 'string', default ='',
                  help = 'bind to tcp addr ADDR', metavar = 'ADDR')
parser.add_option('--tls', dest = 'tls', action = 'store_true', default = False,
                  help = 'enable https mode')
parser.add_option('--cert', dest = 'certfile',
                  help = 'use tls certificate file CERT', metavar = 'CERT')
parser.add_option('--key', dest = 'keyfile',
                  help = 'use tls certificate key file KEY', metavar = 'KEY')
(options, args) = parser.parse_args()

print(f'config: dir="{options.directory}", addr="{options.bind}", port={options.port}, tls={options.tls}')
if options.tls:
    print(f'config: certfile="{options.certfile}", keyfile="{options.keyfile}"')

handler = http.server.SimpleHTTPRequestHandler
server = None
if ':' in options.bind:
    server = HTTPServerV6((options.bind, options.port), handler)
else:
    server = http.server.HTTPServer((options.bind, options.port), handler)
if options.tls:
    context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
    context.load_cert_chain(options.certfile, options.keyfile)
    server.socket = context.wrap_socket(server.socket,
                                        server_side = True)

os.chdir(options.directory)
server.serve_forever()
