#!/bin/sh
#
# configure (isolated) network inside namespace (unshare -r -n)
#

# cfg
dev="net0"
ip4="192.168.42.1"
ip6="fd42::1"

# tools
ip="/sbin/ip"

# for trouble shooting
echo "# my identity"
id -a

# setup everything
if $ip addr ls | grep -q $ip4; then
    echo "# init already done"
else
    echo "# init: lo"
    $ip addr add 127.0.0.1/8 dev lo		|| exit 1
    $ip link set dev lo up			|| exit 1

    echo "# init: $dev (bridge)"
    $ip link add $dev type bridge		|| exit 1
    $ip addr add ${ip4}/24 dev $dev		|| exit 1
    $ip addr add ${ip6}/64 dev $dev		|| exit 1
    $ip link set dev $dev up			|| exit 1

    for i in 0 1; do
	echo "# init: tap$i"
	$ip tuntap add dev tap$i mode tap	|| exit 1
	$ip link set dev tap$i master $dev	|| exit 1
	$ip link set dev tap$i up		|| exit 1
    done
fi

# print results
echo "# show config"
$ip addr ls
