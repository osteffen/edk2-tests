#!/bin/sh

# args
DIR="$1"

# config
CN="$(hostname -f)"
DAYS=7

dn4="boot.v4.net.work"
dn6="boot.v6.net.work"

# common subject
SUB=""
#SUB="${SUB}/C=<country>"
#SUB="${SUB}/ST=<state>"
#SUB="${SUB}/L=<city>"
SUB="${SUB}/O=tls testing"

########################################################################

if test ! -d "$DIR"; then
    echo "usage: $0 <dest-dir>"
    exit 1
fi

# openssl config
cp /etc/pki/tls/openssl.cnf $DIR/openssl.cnf
cat <<EOF >> $DIR/openssl.cnf
[ san ]
subjectAltName=DNS:$dn4,DNS:$dn6
EOF

# create CA
SUBJECT="${SUB}/CN=tls testing cert auth"
echo "### create CA"
openssl genrsa -out "${DIR}/ca.key" 2048
openssl req -x509 -new -nodes -days $DAYS \
	-config "$DIR/openssl.cnf" \
	-key "${DIR}/ca.key" \
	-out "${DIR}/ca.cert" \
	-subj "$SUBJECT"

# create cert
NAME="server"
SUBJECT="${SUB}/CN=${CN}"
echo "### create ${NAME} certificate ($CN)"
openssl genrsa -out "${DIR}/${NAME}.key" 2048
openssl req -new \
	-config "$DIR/openssl.cnf" \
	-key "${DIR}/${NAME}.key" \
	-out "${DIR}/${NAME}.csr" \
	-subj "$SUBJECT" \
	-extensions v3_req \
	|| exit 1
openssl x509 -sha256 -req -days $DAYS \
	-extfile "$DIR/openssl.cnf" \
	-in "${DIR}/${NAME}.csr" \
	-CA "${DIR}/ca.cert" \
	-CAkey "${DIR}/ca.key" \
	-CAcreateserial \
	-extensions san \
	-out "${DIR}/${NAME}.cert" \
	|| exit 1

#echo "### show request"
#openssl req -in "${DIR}/${NAME}.csr" -text

echo "### show certificate"
openssl x509 -in "${DIR}/${NAME}.cert" -text

########################################################################

echo "### create chain (${NAME}-chain.cert)"
cat "${DIR}/${NAME}.cert" "${DIR}/ca.cert" > "${DIR}/${NAME}-chain.cert"

echo "### create efi db (ca.db)"
efisiglist -a -c "${DIR}/ca.cert" -o "${DIR}/ca.db" \
    || exit 1

echo "### show dir"
ls -l "$DIR"
