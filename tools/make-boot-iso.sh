#!/bin/sh

dir="$1"

if test "$dir" = "" -o ! -d "$dir/EFI"; then
    echo "usage: $0 <dir>"
    exit 1
fi

echo "# cleanup in $dir"
rm -v -f "$dir/boot."{img,iso}

echo "# create $dir/boot.img"
export MTOOLS_SKIP_CHECK=1
bootsize=$(du -s --bytes $dir/EFI | awk '{ print $1 }')
bootsize=$(( $bootsize * 12 / 10 / 1024 ))
/usr/sbin/mkdosfs -C "$dir/boot.img" -n efiboot -- $bootsize || exit 1
mcopy -i "$dir/boot.img" -s "$dir/EFI" :: || exit 1
mdir  -i "$dir/boot.img" ::EFI

echo "# create $dir/boot.iso"
xorrisofs \
    -eltorito-boot boot.cat \
    -e "boot.img" \
    -no-emul-boot \
    -o "$dir/boot.iso" \
    \
    -graft-points \
    "$dir/boot.img" \
    EFI="$dir/EFI" \
    "$dir/"*Image \
    "$dir/"rootfs*.xz
