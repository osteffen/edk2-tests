#
# ovmf test cases, boot from network, ia32
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestCore import Edk2TestCore

@avocado.skipUnless(os.path.exists('buildroot/initrd-i386/EFI/BOOT/grub.efi'),
                    'no grub image')
class TestNet(Edk2TestCore):

    timeout = 60

    def common_prepare_ia32_net(self, machine):
        self.common_prepare('i386', machine)
        self.common_add_flash('Build/OvmfIa32/DEBUG_*/FV/OVMF_CODE.fd',
                              'Build/OvmfIa32/DEBUG_*/FV/OVMF_VARS.fd')
        if self.sb_enabled:
            self.cancel("sb is enabled")
        self.common_add_user_net('boot', 'buildroot/initrd-i386',
                                 '/EFI/BOOT/grub.efi')
        
    def test_ovmf_pc_virtio_net_pci(self):
        """
        :avocado: tags=arch:i386
        :avocado: tags=machine:pc
        :avocado: tags=network:virtio-net
        """
        self.common_prepare_ia32_net('pc')
        self.common_add_nic('virtio-net-pci', 'boot')
        self.common_boot_disk()
