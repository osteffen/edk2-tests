#
# redhat distro installer
#

import os
import re
import json
import glob
import shutil
import logging
import tempfile
import configparser
import urllib.error
import urllib.parse
import urllib.request

# avocado
import avocado

# local
from findrepo import findrepo
from Edk2TestCore import Edk2TestCore, PASSWD_FILE, \
    FEDORA_X64, STREAM8_X64, STREAM9_X64, \
    FEDORA_AA64, STREAM8_AA64, STREAM9_AA64, \
    RHEL7_X64, RHEL8_X64, RHEL8_AA64

class DistroInstall(Edk2TestCore):

    timeout = int(os.environ.get("INSTALL_TIMEOUT", "1800"))

    def find_firmware(self):
        if os.environ.get('WORKSPACE') is not None:
            return
        if os.environ.get('EDK2_CODE') is not None:
            return

        list = glob.glob('/usr/share/qemu/firmware/*.json')
        list.sort()
        for item in list:
            f = open(item)
            fw = json.load(f)
            f.close()

            # hard requirements
            if fw['targets'][0]['architecture'] != self.arch:
                continue
            if fw['mapping']['device'] != 'flash':
                continue
            if 'uefi' not in fw['interface-types']:
                continue

            # no secure boot installs for now
            if 'enrolled-keys' in fw['features']:
                continue

            self.log.info('### fw json: ' + item)
            self.log.info('### fw desc: ' + fw['description'])
            os.environ['EDK2_CODE'] = fw['mapping']['executable']['filename']
            os.environ['EDK2_VARS'] = fw['mapping']['nvram-template']['filename']
            return

    def common_prepare_machine(self):
        if self.arch == 'x86_64':
            self.common_prepare('x86_64', 'q35')
            self.common_add_flash('Build/OvmfX64/DEBUG_*/FV/OVMF_CODE.fd',
                                  'Build/OvmfX64/DEBUG_*/FV/OVMF_VARS.fd')
            return

        if self.arch == 'aarch64':
            self.common_prepare('aarch64', 'virt')
            self.common_add_flash('Build/ArmVirtQemu-AARCH64/DEBUG_*/FV/QEMU_EFI.fd',
                                  'Build/ArmVirtQemu-AARCH64/DEBUG_*/FV/QEMU_VARS.fd')
            return

        self.fail('unknown arch')

    def common_prepare_disk(self, image):
        self.log.info(f'### create disk {image} ...')
        cmdline = 'qemu-img create -f qcow2 ' + image + ' 8G'
        avocado.utils.process.run(cmdline)
        blkdev = 'node-name=disk'
        blkdev += ',driver=qcow2'
        blkdev += ',file.driver=file'
        blkdev += ',file.filename=' + image
        self.vm.add_args('-blockdev', blkdev)
        self.common_add_virtio_scsi_pcie()
        self.common_add_disk('scsi-hd')

    def fetch_file(self, repo, file):
        url = repo + file
        req = urllib.request.Request(url)
        self.log.info(f'### fetching {req.full_url} ...')
        try:
            res = urllib.request.urlopen(req)
            return res
        except urllib.error.URLError as err:
            self.log.error(f'### fetch {req.full_url}: {err}')
            self.fail('download failed')
        except Exception as err:
            self.fail('Oops: {err}')

    def prepare_tftp(self, repo):
        bootfiles = [
            f'EFI/BOOT/grub{self.efiarch}.efi',
            self.kernel,
            self.initrd,
        ]
        for item in bootfiles:
            res = self.fetch_file(repo, item)
            out = os.path.join(self.workdir, os.path.basename(item))
            f = open(out, 'wb')
            shutil.copyfileobj(res, f)
            f.close()
            self.rmlist.append(out)

    def prepare_grub_cfg(self, repo):
        out  = os.path.join(self.workdir, 'grub.cfg')
        post = ''
        inst = 'inst.'
        if self.rhel7quirks:
            # old grub.efi, old anaconda ...
            post = 'efi'
            inst = ''

        f = open(out, 'w')
        f.write('echo "grub.efi started"\n')
        f.write(f'echo "install {self.distro}"\n')
        f.write(f'echo "repo {repo}"\n')

        kernel = os.path.basename(self.kernel)
        f.write(f'echo "loading {kernel} ..."\n')
        f.write(f'linux{post} {kernel} console={self.serial} {inst}repo={repo} {inst}ks=file:/ks.cfg\n')

        initrd = os.path.basename(self.initrd)
        f.write(f'echo "loading {initrd} + ks.cpio ..."\n')
        f.write(f'initrd{post} {initrd} ks.cpio\n')

        f.write(f'echo "starting linux kernel ..."\n')
        f.write(f'boot\n')
        f.close()
        self.rmlist.append(out)

    def prepare_kickstart(self, kickstart):
        # read
        f = open(kickstart, 'r')
        cfg = f.read()
        f.close()

        # set password
        f = open(PASSWD_FILE, "r")
        password = f.read().strip()
        f.close()
        cfg = re.sub('rootpw[^\n]*', f'rootpw --plaintext {password}', cfg)

        # write
        out = os.path.join(self.workdir, 'ks.cfg')
        f = open(out, 'w')
        f.write(cfg)
        f.close()

        # wrap into cpio (for initramfs)
        cmdline = f'(cd {self.workdir}; echo ks.cfg | cpio -H newc -o --file=ks.cpio)'
        avocado.utils.process.run(cmdline, shell = True)
        self.rmlist.append(out)
        self.rmlist.append(os.path.join(self.workdir, 'ks.cpio'))

    def distro_info(self):
        self.arch = self.treeinfo['general']['arch']
        if self.arch == 'x86_64':
            self.efiarch = 'x64'
            self.serial = 'ttyS0'
        if self.arch == 'aarch64':
            self.efiarch = 'aa64'
            self.serial = 'ttyAMA0'

        self.distro = self.treeinfo['general']['name']
        self.kernel = self.treeinfo['images-' + self.arch]['kernel']
        self.initrd = self.treeinfo['images-' + self.arch]['initrd']
        self.log.info(f'### distro is {self.distro} ({self.arch})')

        self.rhel7quirks = False
        if self.distro == "Red Hat Enterprise Linux 7.9":
            self.rhel7quirks = True

    def common_run_install(self, repo, image, kickstart):
        res = self.fetch_file(repo, '.treeinfo')
        self.treeinfo = configparser.ConfigParser(allow_no_value=True)
        self.treeinfo.read_string(res.read().decode('utf-8'))
        self.distro_info()
        self.prepare_tftp(repo)
        self.prepare_kickstart(kickstart)
        self.prepare_grub_cfg(repo)

        self.find_firmware()
        self.common_prepare_machine()
        self.vm.add_args('-m', '4G')

        (fh, imagetmp) = tempfile.mkstemp(None,
                                          os.path.basename(image) + '.',
                                          os.path.dirname(image))
        self.rmlist.append(imagetmp)
        os.close(fh)
        self.common_prepare_disk(imagetmp)

        self.common_add_user_net(tftp = self.workdir,
                                 bootfile = f'grub{self.efiarch}.efi')
        self.common_add_nic_pcie('virtio-net-pci',
                                 bootindex = 2)

        self.common_launch()
        self.console_wait('grub.efi started')
        self.log.info(f'### inst: grub loaded')

        self.console_wait('Linux version')
        self.log.info(f'### inst: linux kernel loaded')

        self.console_wait('Starting installer')
        self.log.info(f'### inst: installer loaded')

        self.console_wait('Starting automated install')
        self.log.info(f'### inst: install started')

        if not self.rhel7quirks:
            self.console_wait('Downloading packages')
            self.log.info(f'### inst: rpm downlaod')

        self.console_wait('Preparing transaction')
        self.log.info(f'### inst: rpm install')

        self.console_wait('post-installation setup')
        self.log.info(f'### inst: post install setup')

        if not self.rhel7quirks:
            self.console_wait('Storing configuration files')
            self.log.info(f'### inst: install finished') # almost

        self.console_wait('Rebooting.')
        self.log.info(f'### inst: reboot')

        self.common_linux_distro_login()
        self.common_linux_shell_init()
        self.common_linux_shutdown()
        os.link(imagetmp, image)

    ####################################################################
    # x86_64

    @avocado.skipIf(os.path.exists(FEDORA_X64), 'fedora already installed')
    def test_fedora_x64(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=distro:fedora
        """
        self.common_run_install(findrepo('fedora', '35') + 'Server/x86_64/os/',
                                FEDORA_X64, 'kickstart/fedora-x86_64.ks')

    @avocado.skipIf(os.path.exists(STREAM8_X64), 'centos stream 8 already installed')
    def test_stream8_x64(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=distro:stream8
        """
        self.common_run_install(findrepo('stream8', '8') + 'BaseOS/x86_64/os/',
                                STREAM8_X64, 'kickstart/stream8-x86_64.ks')

    @avocado.skipIf(os.path.exists(STREAM9_X64), 'centos stream 9 already installed')
    def test_stream9_x64(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=distro:stream9
        """
        self.common_run_install(findrepo('stream9', '9') + 'BaseOS/x86_64/os/',
                                STREAM9_X64, 'kickstart/stream9-x86_64.ks')

    @avocado.skipIf(os.path.exists(RHEL7_X64), 'rhel 7 already installed')
    def test_rhel7_x64(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=distro:rhel7
        """
        self.common_run_install(findrepo('rhel7', '7.9') + 'Server/x86_64/os/',
                                RHEL7_X64, 'kickstart/rhel7-x86_64.ks')

    @avocado.skipIf(os.path.exists(RHEL8_X64), 'rhel 8 already installed')
    def test_rhel8_x64(self):
        """
        :avocado: tags=arch:x86_64
        :avocado: tags=machine:q35
        :avocado: tags=distro:rhel8
        """
        self.common_run_install(findrepo('rhel8', '8.5.0') + 'BaseOS/x86_64/os/',
                                RHEL8_X64, 'kickstart/stream8-x86_64.ks')


    ####################################################################
    # aarch64

    @avocado.skipIf(os.path.exists(FEDORA_AA64), 'fedora already installed')
    def test_fedora_aa64(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=distro:fedora
        """
        self.common_run_install(findrepo('fedora', '35') + 'Server/aarch64/os/',
                                FEDORA_AA64, 'kickstart/fedora-aarch64.ks')

    @avocado.skipIf(os.path.exists(STREAM8_AA64), 'centos stream 8 already installed')
    def test_stream8_aa64(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=distro:stream8
        """
        self.common_run_install(findrepo('stream8', '8') + 'BaseOS/aarch64/os/',
                                STREAM8_AA64, 'kickstart/stream8-aarch64.ks')

    @avocado.skipIf(os.path.exists(STREAM9_AA64), 'centos stream 9 already installed')
    def test_stream9_aa64(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=distro:stream9
        """
        self.common_run_install(findrepo('stream9', '9') + 'BaseOS/aarch64/os/',
                                STREAM9_AA64, 'kickstart/stream9-aarch64.ks')

    @avocado.skipIf(os.path.exists(RHEL8_AA64), 'rhel 8 already installed')
    def test_rhel8_aa64(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=distro:rhel8
        """
        self.common_run_install(findrepo('rhel8', '8.5.0') + 'BaseOS/aarch64/os/',
                                RHEL8_AA64, 'kickstart/stream8-x86_64.ks')
