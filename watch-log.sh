#!/bin/sh
tdir=$(ls -dt ~/avocado/job-results/latest/test-results/* | head -1)
exec tail -F ${tdir}/{debug,sercon}.log
