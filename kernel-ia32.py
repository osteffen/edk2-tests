#
# ovmf test cases using direct kernel boot.
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestCore import Edk2TestCore

class TestKernel(Edk2TestCore):

    timeout = 20

    def test_ovmf_ia32_pc_rom(self):
        """
        :avocado: tags=arch:i386
        :avocado: tags=machine:pc
        :avocado: tags=group:kernel
        """
        self.common_prepare('i386', 'pc')
        self.common_add_rom('Build/OvmfIa32/DEBUG_*/FV/OVMF.fd')
        self.common_boot_kernel()

    def test_ovmf_ia32_pc_flash(self):
        """
        :avocado: tags=arch:i386
        :avocado: tags=machine:pc
        :avocado: tags=group:kernel
        """
        self.common_prepare('i386', 'pc')
        self.common_add_flash('Build/OvmfIa32/DEBUG_*/FV/OVMF_CODE.fd',
                              'Build/OvmfIa32/DEBUG_*/FV/OVMF_VARS.fd')
        self.common_boot_kernel()

    def test_ovmf_ia32_q35_rom(self):
        """
        :avocado: tags=arch:i386
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('i386', 'q35')
        self.common_add_rom('Build/OvmfIa32/DEBUG_*/FV/OVMF.fd')
        self.common_boot_kernel()

    def test_ovmf_ia32_q35_flash(self):
        """
        :avocado: tags=arch:i386
        :avocado: tags=machine:q35
        :avocado: tags=group:kernel
        """
        self.common_prepare('i386', 'q35')
        self.common_add_flash('Build/OvmfIa32/DEBUG_*/FV/OVMF_CODE.fd',
                              'Build/OvmfIa32/DEBUG_*/FV/OVMF_VARS.fd')
        self.common_boot_kernel()
