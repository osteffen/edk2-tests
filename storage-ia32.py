#
# ovmf test cases, boot from disk, ia32
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestCore import Edk2TestCore

@avocado.skipUnless(os.path.exists('buildroot/efidisk-i386/efidisk.qcow2'), 'no disk image')
class TestDisk(Edk2TestCore):

    timeout = 60

    def common_prepare_ia32_disk(self, machine):
        self.common_prepare('i386', machine)
        self.common_add_flash('Build/OvmfIa32/DEBUG_*/FV/OVMF_CODE.fd',
                              'Build/OvmfIa32/DEBUG_*/FV/OVMF_VARS.fd')
        if self.sb_enabled:
            self.cancel("sb is enabled")
        self.common_add_qcow2_disk('buildroot/efidisk-i386/efidisk.qcow2')

        
    def test_ovmf_pc_ide(self):
        """
        :avocado: tags=arch:i386
        :avocado: tags=machine:pc
        :avocado: tags=storage:ide
        """
        self.common_prepare_ia32_disk('pc')
        self.common_add_disk('ide-hd')
        self.common_boot_disk()

    def test_ovmf_q35_sata(self):
        """
        :avocado: tags=arch:i386
        :avocado: tags=machine:q35
        :avocado: tags=storage:sata
        """
        self.common_prepare_ia32_disk('q35')
        self.common_add_disk('ide-hd')
        self.common_boot_disk()

    def test_ovmf_pc_virtio_blk_pci(self):
        """
        :avocado: tags=arch:i386
        :avocado: tags=machine:pc
        :avocado: tags=storage:virtio-blk
        """
        self.common_prepare_ia32_disk('pc')
        self.common_add_virtio_blk_pci()
        self.common_boot_disk()

    def test_ovmf_pc_virtio_scsi_pci(self):
        """
        :avocado: tags=arch:i386
        :avocado: tags=machine:pc
        :avocado: tags=storage:virtio-scsi
        """
        self.common_prepare_ia32_disk('pc')
        self.common_add_virtio_scsi_pci()
        self.common_add_disk('scsi-hd')
        self.common_boot_disk()
