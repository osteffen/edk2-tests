#
# armvirt test cases, boot from disk
#

import os
import logging

# avocado
import avocado

# local
from Edk2TestCore import Edk2TestCore

@avocado.skipUnless(os.path.exists('buildroot/efidisk-aarch64/efidisk.qcow2'), 'no disk image')
class TestDisk(Edk2TestCore):

    timeout = 60

    def common_prepare_aa64_disk(self):
        self.common_prepare('aarch64', 'virt')
        self.common_add_flash('Build/ArmVirtQemu-AARCH64/DEBUG_*/FV/QEMU_EFI.fd',
                              'Build/ArmVirtQemu-AARCH64/DEBUG_*/FV/QEMU_VARS.fd')
        self.common_add_qcow2_disk('buildroot/efidisk-aarch64/efidisk.qcow2')

        
    def test_aa64_virtio_blk_pci(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=storage:virtio-blk
        """
        self.common_prepare_aa64_disk()
        self.common_add_virtio_blk_pci()
        self.common_boot_disk()

    def test_aa64_virtio_blk_pcie(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=storage:virtio-blk
        :avocado: tags=io:pcie
        """
        self.common_prepare_aa64_disk()
        self.common_add_virtio_blk_pcie()
        self.common_boot_disk()

    @avocado.skip('broken')
    def test_aa64_virtio_blk_pcie_iommu(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=storage:virtio-blk
        :avocado: tags=io:pcie
        :avocado: tags=io:iommu
        """
        self.common_prepare_aa64_disk()
        self.common_enable_iommu()
        self.common_add_virtio_blk_pcie()
        self.common_boot_disk()

    def test_aa64_virtio_blk_mmio_095(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=storage:virtio-blk
        :avocado: tags=io:mmio
        """
        self.common_prepare_aa64_disk()
        self.vm.add_args('-global', 'virtio-mmio.force-legacy=true')
        self.common_add_virtio_blk_mmio()
        self.common_boot_disk()

    def test_aa64_virtio_blk_mmio_10(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=storage:virtio-blk
        :avocado: tags=io:mmio
        """
        self.common_prepare_aa64_disk()
        self.vm.add_args('-global', 'virtio-mmio.force-legacy=false')
        self.common_add_virtio_blk_mmio()
        self.common_boot_disk()

        
    def test_aa64_virtio_scsi_pci(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=storage:virtio-scsi
        """
        self.common_prepare_aa64_disk()
        self.common_add_virtio_scsi_pci()
        self.common_add_disk('scsi-hd')
        self.common_boot_disk()

    def test_aa64_virtio_scsi_pcie(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=storage:virtio-scsi
        :avocado: tags=io:pcie
        """
        self.common_prepare_aa64_disk()
        self.common_add_virtio_scsi_pcie()
        self.common_add_disk('scsi-hd')
        self.common_boot_disk()

    def test_aa64_virtio_scsi_mmio_095(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=storage:virtio-scsi
        :avocado: tags=io:mmio
        """
        self.common_prepare_aa64_disk()
        self.vm.add_args('-global', 'virtio-mmio.force-legacy=true')
        self.common_add_virtio_scsi_mmio()
        self.common_add_disk('scsi-hd')
        self.common_boot_disk()

    def test_aa64_virtio_scsi_mmio_10(self):
        """
        :avocado: tags=arch:aarch64
        :avocado: tags=machine:virt
        :avocado: tags=storage:virtio-scsi
        :avocado: tags=io:mmio
        """
        self.common_prepare_aa64_disk()
        self.vm.add_args('-global', 'virtio-mmio.force-legacy=false')
        self.common_add_virtio_scsi_mmio()
        self.common_add_disk('scsi-hd')
        self.common_boot_disk()
