#!/bin/bash
# vim: dict+=/usr/share/beakerlib/dictionary.vim cpt=.,w,b,u,t,i,k
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   runtest.sh of /virt/edk2-ovmf
#   Description: run kraxels avocado tests for edk2-ovmf
#   Author: Gerd Hoffmann <kraxel@kraxel.org>
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#   Copyright (c) 2021 Red Hat, Inc.
#
#   This program is free software: you can redistribute it and/or
#   modify it under the terms of the GNU General Public License as
#   published by the Free Software Foundation, either version 2 of
#   the License, or (at your option) any later version.
#
#   This program is distributed in the hope that it will be
#   useful, but WITHOUT ANY WARRANTY; without even the implied
#   warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
#   PURPOSE.  See the GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program. If not, see http://www.gnu.org/licenses/.
#
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# Include Beaker environment
. /usr/bin/rhts-environment.sh || exit 1
. /usr/share/beakerlib/beakerlib.sh || exit 1

PACKAGE="edk2-ovmf"

rlJournalStart
    rlPhaseStartSetup
        rlAssertRpm $PACKAGE
        rlRun "TmpDir=\$(mktemp -d)" 0 "Creating tmp directory"

        toplevel=$(git rev-parse --show-toplevel)
        rlRun "pushd $TmpDir"

        if test -d "$toplevel"; then
            # local
            (cd $toplevel; git archive --prefix=edk2-tests/ HEAD) | tar x
            if test -d $toplevel/buildroot; then
                rlRun "cp -a $toplevel/buildroot edk2-tests"
            fi
        else
            # beaker
            rlRun "git clone --depth 1 https://gitlab.com/kraxel/edk2-tests.git"
        fi
        rlRun "cd edk2-tests"

        # python setup
        python3 -m venv python-venv
        source python-venv/bin/activate
        rlRun "make python" 0 "fetch & install python modules"

        # fetch images
        rlRun "make ARCH=x86_64 extract"
        rlRun "make fetch-grub-x86_64 boot-iso-x86_64"
    rlPhaseEnd

    # avocado tests
    make distros/stream8-x86_64.qcow2
    for file in /usr/share/qemu/firmware/*ovmf*.json; do
        rlPhaseStartTest "testing $(basename $file): $(jq .description $file)"
        rlPhaseEnd
        ./test-rpms.sh $file
    done

    rlPhaseStartCleanup
        rlRun "popd"
        rlRun "rm -r $TmpDir" 0 "Removing tmp directory"
    rlPhaseEnd
rlJournalPrintText
rlJournalEnd
