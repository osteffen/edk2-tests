#!/bin/sh

# arg
distro="$1"

# check
if test ! -f "$distro"; then
    echo "usage: $0 <image.qcow2>"
    exit 1
fi

# tmp qcow2 image
WORK=$(mktemp -d /var/tmp/XXXXXXXX)
trap "rm -rf $WORK" EXIT
qemu-img create -f qcow2 -F qcow2 -b "$(realpath $distro)" "$WORK/disk.qcow2"

# go boot
qemu-efi-x86_64 \
    -name $distro \
    -m 4G \
    -serial stdio \
    -blockdev "node-name=disk,driver=qcow2,file.driver=file,file.filename=$WORK/disk.qcow2" \
    -device virtio-scsi-pci \
    -device scsi-hd,drive=disk
